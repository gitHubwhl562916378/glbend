#version 330
in vec4 vAmbientZM;
in vec4 vDiffuseZM;
in vec4 vSpecularZM;
in vec4 vAmbientFM;
in vec4 vDiffuseFM;
in vec4 vSpecularFM;
out vec4 fragColor;

void main(void)
{
    vec4 finalColor = vec4(0.7,0.5,0.2,1.0);
    if(gl_FrontFacing){
        finalColor = finalColor * (vAmbientZM + vDiffuseZM + vSpecularZM);
    }else{
        finalColor = finalColor * (vAmbientFM + vDiffuseFM + vSpecularFM);
    }
    fragColor = finalColor;
}
