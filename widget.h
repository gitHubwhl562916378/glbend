#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include "mipmaprender.h"
#include "doublelightobjrender.h"
#include "imagerender.h"

class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

private:
    MipMapRender render_;
    DoubleLightObjRender dlRender_;
    ImageRender imageRender_,imageRender1_;
    QMatrix4x4 pMatrix;
    QVector3D camera_,lightLocation_;
};

#endif // WIDGET_H
